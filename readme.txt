Commands to build and deploy the application
```
gcloud builds submit --tag gcr.io/dash-348522/dash-youtube-example  --project=dash-348522

gcloud run deploy --image gcr.io/dash-348522/dash-youtube-example --platform managed  --project=dash-348522 --allow-unauthenticated
```